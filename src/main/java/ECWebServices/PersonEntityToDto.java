package ECWebServices;

import org.springframework.stereotype.Service;

@Service
public interface PersonEntityToDto {
    default PersonEntity PersonEntityToDto(PersonEntity personEntity) {
        return new PersonEntity(
                personEntity.getId(),
                personEntity.getFirstName(),
                personEntity.getLastName(),
                personEntity.getGroups()
        );
    }
}

