package ECWebServices;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class EcWebServicesApplication {

	public static void main(String[] args) {

		SpringApplication.run(EcWebServicesApplication.class, args);
	}
	@Bean
	public CommandLineRunner dummyData(PersonRepository repository){
		return args -> {
			repository.save(new PersonEntity("caroline", "a", null));
			repository.save(new PersonEntity("zakhida", "a", null));
			repository.save(new PersonEntity("jakob", "a", null));
			repository.save(new PersonEntity("teo", "a", null));
			repository.save(new PersonEntity("linnea", "a", null));
			repository.save(new PersonEntity("niclas", "a", null));
		};
	}

}
