package ECWebServices;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.stream.Stream;

@Service
public class PersonService {

    private static final String CODE = "123";

    PersonRepository personRepository;

    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

//    @SneakyThrows
//    public PersonEntity getById(String id){
//        return personRepository.findById(id).orElseThrow(() -> new Exception("person id: " + id +" not found!  "  ));
//    }

    public Stream<PersonEntity> getAllPersons(){
        return personRepository.findAll().stream();
    }

    public PersonEntity getPersonEntity(String id, PersonEntity personDetails) throws Exception {
        PersonEntity personEntity = personRepository.findById(id).orElseThrow( () ->  new Exception("person with id: " + id +" was not found " ));

        personEntity.setFirstName(personDetails.getFirstName());
        personEntity.setLastName(personDetails.getLastName());
        return personRepository.save(personEntity);
    }


    public PersonDTO getPersonDTO(String id) throws Exception {
        PersonEntity person = personRepository.findById(id).orElseThrow(() -> new Exception("person id: " + id +" not found!  "  ));

        PersonDTO dto = new PersonDTO(person.id, person.firstName, person.lastName);

        WebClient client = WebClient.create("http://localhost:8081");

        for (String groupId : person.Groups) {
            GroupDTO group = client.get()
                    .uri("/groups/get/" + groupId)
                    .header("code", CODE)
                    .retrieve()
                    .bodyToMono(GroupDTO.class).block();

            dto.getGroups().add(group);
        }
        return dto;
    }

    public String deletePerson(String membersId) throws Exception {
        PersonEntity person = personRepository.findById(membersId).orElseThrow( () ->  new Exception("person with id: " + membersId +" was not found " ));

        personRepository.delete(person);

        WebClient client = WebClient.create("http://localhost:8081");
        String message = client
                .delete()
                .uri("http://localhost:8081/groups/deleteMember/" + membersId)
                .header("code", CODE)
                .retrieve()
                .bodyToMono(String.class).block();


        return message + ". And person was deleted";
    }

    public PersonDTO addPersonToGroup(String memberId, String groupId) throws Exception {
        PersonEntity person = personRepository.findById(memberId).orElseThrow( () ->  new Exception("group with id: " + memberId +" was not found " ));

        PersonDTO dto = new PersonDTO(person.id, person.firstName, person.lastName);

        WebClient client = WebClient.create("http://localhost:  8081");
        String group = client
                .post()
                .uri("http://localhost:8081/groups/addMember/" + memberId + "/" + groupId)
                .header("code", CODE)
                .retrieve()
                .bodyToMono(String.class).block();
        //instead of getting message only respone make groupDTO with list<Strings> to be able to return better response

        return dto;
    }
}
