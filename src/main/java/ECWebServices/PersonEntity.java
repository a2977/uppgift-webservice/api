package ECWebServices;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;
import java.util.UUID;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity(name="person")
public class PersonEntity {
    @Id
    String id ;
    String firstName;
    String lastName;

    @ElementCollection
    protected List<String> Groups;

    public PersonEntity(String firstName, String lastName, List<String> groups) {
        this.id = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
        Groups = groups;
    }
    public void addGroup(GroupDTO group) {
        Groups.add(group.getId());
    }

    void deleteMember(GroupDTO id){
        for(String memberId : Groups){
            if(memberId.equals(id)){
                Groups.remove(id);
            }
        }
    }
}
