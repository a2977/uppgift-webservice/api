package ECWebServices;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Getter
public class GroupDTO {
    String id;
    String groupName;
    protected List<PersonDTO> groupMembers;

    public GroupDTO(String groupName) {
        this.id = UUID.randomUUID().toString();
        this.groupName = groupName;
        this.groupMembers = new ArrayList<>();
    }
}
