package ECWebServices;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@RestController
@RequestMapping("/person")
@AllArgsConstructor
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class PersonController {
    PersonRepository repository;
    PersonService personService;


    private static final String CODE = "123";
    @GetMapping("/all")
    public List<PersonEntity> GetAllStudent(){
        return repository.findAll();
    }

    @GetMapping("/get/{id}")
    public ResponseEntity<PersonDTO> getPersonById(@PathVariable(value= "id")String id ) throws Exception {
        PersonDTO dto = personService.getPersonDTO(id);
        return ResponseEntity.ok().body(dto);
    }


    @PostMapping("/create")
    public PersonEntity createPerson(@Valid @RequestBody PersonEntity personDetails){
        personDetails.setId(UUID.randomUUID().toString());
        personDetails.setGroups(List.of());
        return repository.save(personDetails);
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<PersonEntity> updatePerson(@PathVariable( value="id") String id, @Valid @RequestBody PersonEntity personDetails) throws Exception {
        final PersonEntity updatePerson = personService.getPersonEntity(id, personDetails);
        return ResponseEntity.ok(updatePerson);
    }


    @DeleteMapping("/delete/{id}")
    public String deletePerson(@PathVariable(value ="id") String id) throws Exception {

        return personService.deletePerson(id);
    }


    @PostMapping("/addGroup/{groupId}/{personId}")
    public PersonEntity addGroupToPerson(@PathVariable(value = "groupId") String groupId, @PathVariable(value = "personId") String personId) throws Exception {
        PersonEntity person = repository.findById(personId).orElseThrow(() -> new Exception("group id: " + personId + " not found!  "));
        person.getGroups().add(groupId);

        personService.addPersonToGroup(personId, groupId);

        return repository.save(person);
    }
}


